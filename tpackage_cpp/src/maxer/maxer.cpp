#include "ros/ros.h"
#include "std_msgs/Int64.h"
#include "tpackage_cpp/CalculateReq.h"
#include <thread>
#include <chrono>
#include <vector>
#include <cstdlib>
#include <algorithm>
#include "boost/chrono.hpp"
#include "boost/chrono/duration.hpp"
#include "boost/algorithm/string.hpp"
#include "maxer/maxer.hpp"
#include <string>

const int64_t MAXINT = 111000;

using std::vector;
using std::string;

Maxer::Maxer(ros::NodeHandle node) {
    this->pub = node.advertise<std_msgs::Int64>("/max", 10);
}

/**
 * Sets the subscriber, which will receive numbers for processing
 **/
void Maxer::setNumberSubscriber(const ros::Subscriber& sub) {
    this->sub = sub;
}

void Maxer::callback(const tpackage_cpp::CalculateReq& req) {
    // keep in mind, that ROS initializes nodes in such a shitty
    // way, that the subscriber may initialize later than
    // the messages will be sent to the topic
    std::this_thread::sleep_for(std::chrono::milliseconds(500));

    std_msgs::Int64 msg;
    string numstr = string(req.nums.c_str());
    msg.data = this->max(this->parse(numstr));

    this->pub.publish(msg);
}

int64_t Maxer::max(vector<int64_t> nums) {
    int64_t res = -MAXINT;
    for (const int64_t& num : nums) {
        res = std::max(res, num);
    }
    return res;
}

vector<int64_t> Maxer::parse(string& s) {
    ROS_DEBUG("received string '%s'", s.c_str());

    int64_t num = -MAXINT;
    try {
        num = std::stoi(s.c_str());
    } catch (std::exception const& e) {
        num = -MAXINT;
    }

    if (num != -MAXINT) {
        return vector<int64_t>(1, num);
    }

    if (s.size() < 1) {
        return vector<int64_t>(0);
    }

    if (s[0] == '[' && s[s.size() - 1] == ']') {
        s.pop_back();
        s.erase(s.begin());
    }

    vector<string> strs;
    string splitter = " ";

    if (s.find(" ")) {
        splitter = ",";
    }

    boost::split(strs, s, boost::is_any_of(splitter));

    vector<int64_t> res;

    for (const string& str : strs) {
        try {
            num = std::stoi(str.c_str());
        } catch (const std::exception& e) {
            ROS_ERROR("failed to convert %s as number", str.c_str());
            continue;
        }
        res.push_back(num);
    }

    return res;
}
