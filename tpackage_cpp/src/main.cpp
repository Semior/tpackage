#include "ros/ros.h"
#include "maxer/maxer.hpp"

int main(int argc, char** argv) {
    ros::init(argc, argv, "maxer");
    ros::NodeHandle n;

    Maxer m = Maxer(n);
    ros::Subscriber sub = n.subscribe<tpackage_cpp::CalculateReq>(
        "/numbers", 1000, [&](const tpackage_cpp::CalculateReq::ConstPtr& req) { m.callback(*req.get()); });
    m.setNumberSubscriber(sub);
    ros::Rate rate(10);

    while (ros::ok()) {
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}
