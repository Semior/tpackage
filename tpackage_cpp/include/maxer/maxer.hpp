#include "tpackage_cpp/CalculateReq.h"
#include "ros/ros.h"
#include <vector>
#include <string>

#ifndef _maxer_included
#define _maxer_included

using std::vector;
using std::string;

class Maxer {
  public:
    Maxer(ros::NodeHandle node);
    void setNumberSubscriber(const ros::Subscriber& sub);
    void callback(const tpackage_cpp::CalculateReq& req);

  private:
    ros::Subscriber sub;
    ros::Publisher pub;
    vector<int64_t> parse(string& s);
    int64_t max(vector<int64_t> nums);
};
#endif