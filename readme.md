# ROS package for demonstrating "how to write unit test"

[![pipeline status](https://gitlab.com/Semior/tpackage/badges/master/pipeline.svg)](https://gitlab.com/Semior/tpackage/commits/master) [![coverage report](https://gitlab.com/Semior/tpackage/badges/master/coverage.svg)](https://gitlab.com/Semior/tpackage/commits/master)

# Disclaimer

Author of this package does not know anything about writing unit tests in ROS and about development of ROS packages.

Commands for execution:

```bash
catkin build                # compiling messages and other necessaries
source devel/setup.bash      # adding build artifacts to the bash environment
catkin run_tests            # running tests
catkin_test_results 
```

- [General unit testing theory](doc/unit_testing.md)
- [ROS specifics in writing unit tests](doc/unit_testing_in_ros.md)

# Running vscode (vscode server) inside docker container
```bash
# build the minimum development image
docker build -t ros_min .

# run the container, assuming that you're inside the package directory
# it runs the container in foreground and detached
docker run --cap-add=SYS_PTRACE -t -d -v $(pwd):/root/catkin_ws/src ros_min
#
# > i suggest you to do not add --rm flag, instead, reuse the container
# > it just will take a long time for you to wait until vscode installs its server
# > inside of it (please make mr if you find a suitable way to preinstall it via Dockerfile)
#
# inside vscode run "Remote-Containers: Attach to the existing container" or smth
# and choose the /root/catkin_ws/src as a workspace folder
```
After launching the vscode workspace, you may need to add additional parameters for vscode to index ROS packages.
In `${workspace_name}/.vscode/settings.json` there should be a parameter:
```json
"python.autoComplete.extraPaths": [
    "/opt/ros/melodic/lib/python2.7/dist-packages",
    "/home/app/catkin_ws/devel/lib/python2.7/dist-packages"
],
```
This will make your vscode instance index the built packages.

# debugging
[ms-iot/vscode-ros](https://github.com/ms-iot/vscode-ros/) supports debugging by attaching to the running processes
