#!/usr/bin/env python3
import unittest

import rospy
import rosunit

# FIXME no module named scripts

from tpackage.src.maxer import Maxer

PACKAGE = "tpackage"
NODE_NAME = "basic_unit_test"


class FakePublisher(rospy.Publisher):
    val = None

    def __init__(self):
        pass

    def publish(self, *args, **kwds):
        self.val = args

    def get_num_connections(self):
        pass

    def unregister(self):
        pass


class BasicUnitTest(unittest.TestCase):
    """Each test has a prefix 'test' in its name"""
    def setUp(self):
        """setUp method is called every time, while test is running"""
        self.maxer = Maxer(FakePublisher())

    def test_parser_normal_cases(self):
        self.assertEquals(
            self.maxer.parse("[10, 3, 0, 1, 8]"),
            [10, 3, 0, 1, 8],
            "parser, normal case: [10, 3, 0, 1, 8]"
        )

        self.assertEquals(
            self.maxer.parse("[0, 0, 0, 1, 90]"),
            [0, 0, 0, 1, 90],
            "parser, normal case: [0, 0, 0, 1, 90]"
        )

    def test_parser_extreme_cases(self):
        self.assertEquals(
            self.maxer.parse("[3, 3, 3, 3, 3, 3]"),
            [3, 3, 3, 3, 3, 3],
            "parser, extreme case: [3, 3, 3, 3, 3, 3]"
        )

        self.assertEquals(
            self.maxer.parse("2"),
            [2],
            "parser, extreme case: 2"
        )

        self.assertEquals(
            self.maxer.parse("[]"),
            [],
            "parser, error case: []"
        )

        self.assertEquals(
            self.maxer.parse("['aaa', 3, nan, None]"),
            [3],
            "parser, error case: 3"
        )

    def test_max_normal_cases(self):
        self.assertEquals(
            self.maxer.max([10, 3, 0, 1, 8]),
            10,
            "maxer, normal case: [10, 3, 0, 1, 8]"
        )

        self.assertEquals(
            self.maxer.max([0, 0, 0, 1, 90]),
            90,
            "maxer, normal case: [0, 0, 0, 1, 90]"
        )

    def test_max_extreme_cases(self):
        self.assertEquals(
            self.maxer.max([3, 3, 3, 3, 3, 3]),
            3,
            "maxer, extreme case: [3, 3, 3, 3, 3, 3]"
        )

        self.assertEquals(
            self.maxer.max([2]),
            2,
            "maxer, extreme case: [2]"
        )

    def test_max_error_case(self):
        self.assertEquals(
            self.maxer.max([]),
            -111000,
            "maxer, error case: []"
        )


if __name__ == '__main__':
    rospy.init_node(NODE_NAME)
    rosunit.unitrun(
        PACKAGE,
        NODE_NAME,
        BasicUnitTest,
        coverage_packages=[PACKAGE],
        sysargs=["--with-coverage", "--cov", "--covhtml"]
    )
