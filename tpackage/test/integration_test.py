#!/usr/bin/env python3
import time
import unittest
import rospy
import rosunit
from std_msgs.msg import Int64
from tpackage.msg import CalculateReq

PACKAGE = "tpackage"
NODE_NAME = "integration_test"
TOPIC_NAME = "/max"


class BasicTest(unittest.TestCase):
    """Each test has a prefix 'test' in its name"""
    def setUp(self):
        """setUp method is called every time, while test is running"""
        self.pub = rospy.Publisher("/numbers", CalculateReq, queue_size=10)
        # ROS does not have enough time to make a publisher, that's why
        #  test_error_cases did not finish properly WTF?!?!?!??!
        #  I just fucked this shit, I spent about 18 hours on solving it
        time.sleep(1)

    def test_normal_cases(self):
        self.pub.publish("[10, 3, 0, -1, 8]")
        try:
            # noinspection PyTypeChecker
            data = rospy.wait_for_message(
                TOPIC_NAME, Int64, timeout=3
            )  # type: Int64
        except rospy.ROSException:
            self.fail("Timeout - node did not send anything within 3 seconds")
        self.assertEquals(10, data.data)

        self.pub.publish("[0, 0, 0, -1, 90]")
        try:
            # noinspection PyTypeChecker
            data = rospy.wait_for_message(
                TOPIC_NAME, Int64, timeout=3
            )  # type: Int64
        except rospy.ROSException:
            self.fail("Timeout - node did not send anything within 3 seconds")
        self.assertEquals(90, data.data)

    def test_extreme_cases(self):
        self.pub.publish("[3, 3, 3, 3, 3, 3]")
        try:
            # noinspection PyTypeChecker
            data = rospy.wait_for_message(
                TOPIC_NAME, Int64, timeout=3
            )  # type: Int64
        except rospy.ROSException:
            self.fail("Timeout - node did not send anything within 3 seconds")
        self.assertEquals(3, data.data)

        self.pub.publish("2")
        try:
            # noinspection PyTypeChecker
            data = rospy.wait_for_message(
                TOPIC_NAME, Int64, timeout=3
            )  # type: Int64
        except rospy.ROSException:
            self.fail("Timeout - node did not send anything within 3 seconds")
        self.assertEquals(2, data.data)

        self.pub.publish("['aaa', 3, nan, None]")
        try:
            # noinspection PyTypeChecker
            data = rospy.wait_for_message(
                TOPIC_NAME, Int64, timeout=3
            )  # type: Int64
        except rospy.ROSException:
            self.fail("Timeout - node did not send anything within 3 seconds")
        self.assertEquals(3, data.data)

    def test_error_cases(self):
        self.pub.publish("[]")
        try:
            # noinspection PyTypeChecker
            data = rospy.wait_for_message(
                TOPIC_NAME, Int64, timeout=3
            )  # type: Int64
        except rospy.ROSException:
            self.fail("Timeout - node did not send anything within 3 seconds")
        self.assertEquals(-111000, data.data)


if __name__ == '__main__':
    rospy.init_node(NODE_NAME)
    rosunit.unitrun(
        PACKAGE,
        NODE_NAME,
        BasicTest
    )
