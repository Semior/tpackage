#!/usr/bin/env python3
"""Basic module for demonstrating tests writing and usage in ROS development

WARNING: pep257 assumes, that all first lines of docstrings wrote in present
simple, as an impersonal sentence
"""
import time

import rospy
import re


class Maxer:
    """Basic Maxer class

    Takes integers from one topic and returns the max number of
    them to another
    """

    def __init__(self, pub):
        """Initialize of Maxer class

        Initializes subscriber for taking numbers and publisher for returning
        the max of them
        :param pub:               publisher, which will send the result to the
                                    topic
        :type pub:                rospy.Publisher
        """
        self.pub = pub
        self.number_subscriber = None

    def set_number_subscriber(self, number_subscriber):
        """Set number subscriber

        :param number_subscriber: subscriber, which will receive numbers for
                                    processing
        :type number_subscriber:  rospy.Subscriber
        """
        self.number_subscriber = number_subscriber

    def callback(self, data):
        """Transfer callback data to the processing function

        Passes the data from rostopic message to the function that processes
        these data and publishes the result to another topic
        :param data: Int64MultiArray
        :return: None
        """
        # Subscriber in integration test just does not have enough time
        #  for reading message WTF?!?!?!?!?
        #  I just fucked this shit, I spent about 18 hours on solving it
        time.sleep(0.5)
        self.pub.publish(self.max(self.parse(data.nums)))

    def parse(self, s):
        """Convert string of numbers to the list of numbers

        :param s: string, which contains numbers. Example: "[1 2 3 4 5]"
        :type s: str
        :return: list[int]
        """
        if s.isdigit():
            return [int(s)]

        if len(s) < 1:
            return []

        if s[0] == '[' and s[-1] == ']':
            s = s[1:len(s) - 1]

        nums_in_str = []
        if s.find(',') != -1 and s.find(' ') == -1:
            nums_in_str = s.split(',')
        else:
            nums_in_str = s.split(' ')

        for i in range(0, len(nums_in_str)):
            nums_in_str[i] = re.sub("[^0-9]", '', nums_in_str[i])

        return [int(i) for i in nums_in_str if i.isdigit()]

    def max(self, nums):
        """Calculate the max number from given numbers and return it

        :param nums:
        :return: int
        """
        if len(nums) < 1:
            return -111000

        res = nums[0]
        for i in nums[1:]:
            res = max(res, i)
        return res


def main():
    """Initialize node and Maxer instance."""
    from std_msgs.msg import Int64
    from tpackage.msg import CalculateReq

    rospy.init_node("maxer")
    m = Maxer(
        pub=rospy.Publisher("/max", Int64, queue_size=10)
    )
    m.set_number_subscriber(rospy.Subscriber(
        "/numbers",
        CalculateReq,
        m.callback
    ))
    rate = rospy.Rate(10)

    while not rospy.is_shutdown():
        rate.sleep()


if __name__ == '__main__':
    main()
