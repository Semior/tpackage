#!/usr/bin/env python3
"""Module for passing numbers from keyboard to rostopic

WARNING: pep257 assumes, that all first lines of docstrings wrote in present
simple, as an impersonal sentence
"""


class Reader:
    """Reader reads values from keyboard and sends them to /numbers topic."""

    def __init__(self, pub):
        """Initialize reader."""
        self.pub = pub

    def read_and_send(self):
        """Read values from keyboard and send them to the topic."""
        nums = input("values (separated by space): ")
        self.pub.publish(nums)


def main():
    """Initialize node and Reader instance."""
    from tpackage.msg import CalculateReq
    import rospy

    rospy.init_node("reader")
    reader = Reader(rospy.Publisher("/numbers", CalculateReq, queue_size=10))
    rate = rospy.Rate(10)

    while not rospy.is_shutdown():
        reader.read_and_send()
        rate.sleep()


if __name__ == '__main__':
    main()
