FROM osrf/ros:melodic-desktop-full-bionic

# installing dependencies
RUN apt-get update && apt-get install -q -y \ 
    python-catkin-tools\
    python3-pip \
    python-pip \
    gdb \
    clang-format-3.9 \
  && pip3 install \
    rospkg \
    pyyaml \
  && rm -rf /var/lib/apt/lists/* \
  && sh -c "$(curl --location https://taskfile.dev/install.sh)" -- -d -b /usr/local/bin

# initialize catkin_ws
RUN /bin/bash -c "source /opt/ros/melodic/setup.bash && \
                  mkdir -p ~/catkin_ws/src && \
                  cd ~/catkin_ws/src && \
                  catkin_init_workspace && \
                  cd ~/catkin_ws/ && \
                  catkin build --no-status && \
                  echo 'source ~/catkin_ws/devel/setup.bash' >> ~/.bashrc &&\
                  echo 'source /usr/share/gazebo/setup.sh' >> ~/.bashrc"
