# What is unit?
In terms of unit testing, the **unit** is a piece of code (usually a function) which dependencies might be either [mocked](#what-is-mock) or rid off and run in a [unit test](#what-is-unit-test)

# What is unit test?
It is a piece of code (usually a function) which runs the [unit](#what-is-unit) and asserts some statements about this function.

# What is mock?
It is a stub, which might be a class, function or another piece of code that might be placed instead of required dependency in [unit](#what-is-unit)

# How does look the simple unit test?
Without working with any unit-test framework, library and so on, in python the simpliest unit test looks this way:
```python
def a_plus_b(a: int, b: int) -> int:
    return a + b

def unit_test():
    a = 5
    b = 3
    if a_plus_b(a, b) != 8:
        print('unit test failed')
        return
    print('unit test passed')
    return
```

See? Nothing really hard, actually. So, we have a unit - a function that we would like to test and a unit test - another function, that calls the unit and asserts that its output is correct.

# Why do we need unit tests?
Basically, the unit tests is not only about the **testing** application, but it could be also used to **plan** the application. 

There is such an approach in application development, called TDD - Test Driven Development. This approach might be described in phrase "Test first, code second". 

## What does it mean?

Basically, the simpliest way to plan (at least, this approach I use) is to write a test, that asserts the **correct** output of the unit, instead of trying to test all the ways that the function can do.

## What is a test coverage?

A test coverage is a relation of the instructions that were run during the unit test to the total number of instructions.

By instructions I mean the runnable code lines, that performs some action. Example:

```python
if a != b:
    return True
else:
    return False
```

This is an instruction with 3 lines. Why? Because the `else:` line refers to the `if` statement, other two lines are returns and they might be executed.

```python
class Test:
    def test(self):
        return
```

This is a piece of code that contains 1 instruction - return. Remember, the definition of class and definition of function are not an instructions! They cannot be run during the test, as they are only declarations.

## How does it look?

Let's take an example of simpliest unit that we would like to write. Let's make a function that parses the geometry dimensions from string in form "*w***x***h*" (example "5x6", "7x8") and throws an exception if the string format is invalid or the dimensions are less or equal zero.

First, we have to define a name of our function. Let it be `parse_geometry`. So, firstly we make a stub:

```python
from typing import Tuple
def parse_geometry(s: str) -> Tuple[int, int]:
    return 0, 0

# By the way, it is always a good practice to annotate the types of an input and output of functions.
```

Next, we would like to write a unit test, covering the correct way of use the function:
```python
def unit_test():
    a, b = parse_geometry('5x6')
    if a != 5:
        print('unit test failed, expected a={}, got a={}'.format(5, a))
        return
    if b != 6:
        print('unit test failed, expected b={}, got b={}'.format(6, b))
        return
```

Now, we have a unit test, that checks that the `parse_geometry` function returns the correct output on a correct values. Let's edit our stub to make a function to do so:
```python
from typing import Tuple
def parse_geometry(s: str) -> Tuple[int, int]:
    parts = s.split('x')
    return int(parts[0]), int(parts[1])
```

Good, but what if we will try to parse an incorrect string, like "12345" or "12x3x5"? Let's make a condition that checks that the input value is correct:
```python
from typing import Tuple
def parse_geometry(s: str) -> Tuple[int, int]:
    parts = s.split('x')
    if len(parts) != 2:
        raise Exception('the input string is incorrect')

    return int(parts[0]), int(parts[1])
```
Looks better, right? But what if the dimensions are negative or equal zero? Let's also add such a check into our function:
```python
from typing import Tuple
def parse_geometry(s: str) -> Tuple[int, int]:
    parts = s.split('x')
    if len(parts) != 2:
        raise Exception('the input string is incorrect')
    w, h = int(parts[0]), int(parts[1])
    if w <= 0 or h <= 0:
        raise Exception('some of input value is non-positive')
    return w, h
```
Good! Now, we have our working function. But what about the test? We did not cover all the possible ways of the use of our function.

Answer: suprise! It is not necessary, to cover all of them and spend a lot of time on it. But! Most of developers, that writes unit tests like when their code is much enough covered, as the code coverage shows that the code is mostly stable and almost enough tested. So, we may either leave the test as it is, as it already done with its goal - plan the code, or we may extend it to cover the whole function. Let's do the second variant.

```python
def unit_test():
    a, b = parse_geometry('5x6')
    if a != 5:
        print('unit test failed, expected a={}, got a={}'.format(5, a))
        return
    if b != 6:
        print('unit test failed, expected b={}, got b={}'.format(6, b))
        return
    
    # incorrect string format
    try:
        a, b = parse_geometry('12345')
        print('unit test failed, expeceted an exception, got nothing')
        return
    except:
        pass
    
    # incorrect dimensions
    try:
        a, b = parse_geometry('0x0')
        print('unit test failed, expeceted an exception, got nothing')
        return
    except:
        pass

    print('unit test passed')
    return
```

Alright, we've done with it.

But wait! We did not cover all possible variants? We have to test it with multiple input values!

Answer: no, we don't. Our primary goal was to extend the **coverage** and to not make the test an overkill unless it checks some important branches of the testing function. Right now the coverage is 100%, so we may just leave the test as it is as no other variants of use are expected.

## Mocking

Assume that we have a class `AuthService` which method `check_user_credentials` we would like to test:
```python
class AuthService:
    def __init__(self, db): # db describes another service-class, basically DAO (Data Access Object) which provides methods to access the data in the database
        self.__db = db
    
    def check_user_credentials(self, username: str, password: str) -> bool:
        # Assume that this method should check the user credentials. Checking user credentials also requires the intermediate step of hashing the input password and comparing this hash with the stored one as we don't store the password in the database as a plain text.
        usr = self.__db.get_user(username)
        if usr == None:
            return False
        
        if usr.password != hash_password(password):
            return False
        
        return True
    
def hash_password(pwd: str) -> str:
    # there should be some hashing magic
    pass
```

So, the method `check_user_credentials` contains a dependency - database service-class `self.__db`. As we would not like to directly access the database during the test (as the DAO is not a part of our currently testing unit), we would like to **mock** it.

The mock is just a stub, that always (unless it is required by test) returns the correct data. So, we would simply mock it as:
```python
class MockDB:
    def __init__(self, stub_username: str, stub_pwd: str):
        self.__username = stub_username
        self.__pwd = stub_pwd

    def get_user(self, username) -> User:
        if username != self.__username:
            return None
        return User(self.__username, self.__pwd)

class User:
    # ...
    pass
```

Now, we may simply use it in test this way:
```python
def unit_test():
    srv = AuthService(MockDB("semior001", "some_awesome_pwd"))
    if not srv.check_user_credentials("semior001", "some_awesome_pwd"):
        print("unit test failed")
        return
    
    if srv.check_user_credentials("mister_pomodoro", "blah"):
        print("unit test failed")
        return
    
    print("unit test passed")
    return
```

# Workning with testing frameworks

The python contains a built-in package for testing purposes, called `unittest`. It contains predefined assertions and helpers to test. It is really huge and I won't describe all its method and functions as the [documentation](https://docs.python.org/3/library/unittest.html) will do it better and quicker.


That's it with the general theory of writing unit tests. ROS-specifics in writing tests you may find [here](unit_testing_in_ros.md).