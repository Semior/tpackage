# Example of unit test
You may create python unit test anywhere (unless it is defined by your mentor), the name of python script is also unimportant. Here is a sample of basic unit test:

```python
#!/usr/bin/env python
import sys
import unittest
import rosunit

class BasicTest(unittest.TestCase):
    """
    Basic test class
    """
    def setUp(self):
        
    def test_one_equals_one(self):
        self.assertEquals(True, False, "1!=1")

if __name__ == '__main__':
    rosunit.unitrun("base", 'test_bare_bones', BasicTest)
```

This line just checks that True is equal to False, if it is not so, it will print "1!=1":
```python
self.assertEquals(True, False, "1!=1")
```

This line declares a class, which consist of unit tests:
```python
class BasicTest(unittest.TestCase):
```

This line says to ROS that this is a unit test of package named "base", and that ROS should run tests inside BasicTest class and the second argument of function "unitrun" is just name of the test, which will be shown in XML reports:
```python
rosunit.unitrun("base", 'test_bare_bones', BasicTest)
```

# Running unit tests
Before running test, you have to create a .test file anywhere in your package:

```xml
<launch>
    <node pkg="tpackage" name="maxer" type="maxer.py" output="screen" />
    <test pkg="tpackage" test-name="basic_test" type="basic_test.py" />
</launch>
```
As you can see, the syntax is the same as for .launch file. In .test file, you have to declare the test itself, and the node, that will be tested.

Before running tests, you have to add the following string for each test inside CMakeLists.txt inside your ros-package:
```
add_rostest(path/to/your/.test) 
```

For running unit test, you have to enter to catkin workspace directory, execute the next command:
```bash
catkin run_tests
```

This command will build the project and run tests. You may find output and results of each test inside the output of this command, but it is simpler to show it via next sequence of commands:

```bash
source devel/setup.bash
catkin_test_results
```

It will print results of all test scripts inside your project
